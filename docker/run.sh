#!/bin/bash
JUPYTER_TOKEN='sail'

NAME=container-example
IMAGE=test-lib-got
docker run -it -d -p 8888:8888 -v $PWD:/root/example_python_got -e JUPYTER_TOKEN=${JUPYTER_TOKEN} --name ${NAME} ${IMAGE}
