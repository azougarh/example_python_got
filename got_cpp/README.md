### library for tracking (C++17).
#### requirements
all requirement are downloaded and installed within the docker file and cmake.
list of requirements:  


* opencv4  
* Boost::program_options  
* googletest  
* spdlog  
* libgtk2.0-dev  

#### hands on and setup docker environment 
* git clone
* `git lfs pull`
* `cd Docker`
* `./run_docker.sh`
* `./build.sh`
#### build
* `mkdir build && cd build`
* `cmake .. && make -j8`
#### run tests
* `cd build`
* change root path in `/tracker/config/tracker_conf.ini`
* `./tests`

### Features
* handles initialization of tracker matrices using `treacker_conf.ini` (default values provided). 
* mutli-objects tracking of bounding boxes using linear kalman filter.
* example usage available in `main.cpp`





###### setup docker env
* `apt -y install libgtk2.0-dev libtbb-dev qt5-default`
* `git clone https://github.com/opencv/opencv.git` (master) [src](https://www.learnopencv.com/install-opencv-4-on-ubuntu-18-04/).
* `git clone https://github.com/gabime/spdlog.git`
