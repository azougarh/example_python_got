//
// Created by azougarh on 13/02/2020.
//

#include "../tracker/include/structs.h"
#include <fstream>
#include <iostream>
#include <iterator>
#include <opencv4/opencv2/opencv.hpp>
#include <spdlog/spdlog.h>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

vector<string> split(string &s, string delimiter) {
  vector<string> elems;
  size_t pos = 0;
  string token;
  while ((pos = s.find(delimiter)) != string::npos) {
    token = s.substr(0, pos);
    elems.push_back(token);
    s.erase(0, pos + delimiter.length());
  }
  if (s.length()) {
    elems.push_back(s);
  }
  return elems;
}

void import_single_detection(std::ifstream &current,
                             std::vector<PositionInFrame> &detections,
                             int n_detections) {
  //spdlog::debug("parsing detections file ..") ;
  string line;
  for (int j = 0; j < n_detections; j++) {
    getline(current, line);
    vector<string> coordinates = split(line, " ");

    int x = stoi(coordinates[0]);
    int y = stoi(coordinates[1]);
    int w = stoi(coordinates[2]);
    int h = stoi(coordinates[3]);

    cv::Rect bbox = cv::Rect(x, y, w, h);
    PositionInFrame position = PositionInFrame(bbox);

    detections.push_back(position);
  }
}

vector<vector<PositionInFrame>> import_detections(string input,
                       vector<vector<PositionInFrame>> &all_regions,
                       int number_frames) {
  for (int i = 1; i < number_frames + 1; i++) {
    // spdlog::debug("import detection {}.txt", i);
    string detection_name = std::to_string(i) + ".txt";
    std::ifstream current(input + detection_name);

    // check that the frame number is same as current frame
    string current_name;
    getline(current, current_name);

    assert(stoi(current_name) == i);

    // get number of detections in file
    string n_detections;
    getline(current, n_detections);

    // detections in the current frame
    std::vector<PositionInFrame> detections = std::vector<PositionInFrame>();

    import_single_detection(current, detections, stoi(n_detections));

    all_regions.push_back(detections);
  }
  return all_regions;
}


void displayFrameNb(cv::Mat &frame, int number) {
  cv::putText(frame, std::to_string(number), cv::Point(30, 30), cv::FONT_HERSHEY_DUPLEX, 1, cv::Scalar(0, 190, 0), 4);
}

// TODO test this iou
double getIOU(PositionInFrame p1, PositionInFrame p2) {
  cv::Rect rect1 = p1.toRect();
  cv::Rect rect2 = p2.toRect();

  int x1_left = rect1.x;
  int x1_right = rect1.x + rect1.width;
  int x2_left = rect2.x;
  int x2_right = rect2.x + rect2.width;
  int y1_top = rect1.y;
  int y1_bottom = rect1.y + rect1.height;
  int y2_top = rect2.y;
  int y2_bottom = rect2.y + rect2.height;

  spdlog::debug("lefts x {0}, {1}, rights x {2}, {3}", x1_left, x2_left, x1_right, x2_right);

  int overlap_width = min(x1_right, x2_right) - max(x1_left, x2_left);
  int overlap_height = min(y1_bottom, y2_bottom) - max(y1_top, y2_top);
  spdlog::debug("overlap width {0}, overlap height {1}", overlap_width, overlap_height);

  int _overlap = 0 ;
  if (overlap_height > 0 && overlap_width > 0)
    _overlap = overlap_height * overlap_width;

  double _union = rect1.area() + rect2.area() - _overlap;
  spdlog::debug("overlap value is {0}, union is {1}", _overlap, _union);
  return _overlap / _union;
}
