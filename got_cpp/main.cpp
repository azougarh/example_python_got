
#include "tracker/include/KalmanExtendedCTRV.h"
#include "tracker/include/KalmanLinearConstantVelocity.h"
#include "tracker/src/MTracker.cpp"  // always include cpp when using template classes
#include "utils/utils.h"
#include <experimental/filesystem>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <spdlog/spdlog.h>

using namespace std;
using namespace MTracker;
using namespace boost;


int main_ekf() {
  spdlog::set_level(spdlog::level::debug);

  TrackerConfig config = TrackerConfig("../tracker/config/ekf_tracker_conf.ini");
  //KalmanLinearConstantVelocity tk = KalmanLinearConstantVelocity(config.getCovParamsValues());
  KalmanExtendedCTRV tk = KalmanExtendedCTRV(config.getCovParamsValues());

  spdlog::info("importing detections");
  std::string detections_path = "/home/user/got_cpp/data/islanp_face_moving/";
  int nb_frames = 462;

  int width = 1280;
  int height = 720;
  int fps = 30; //25;
  double dT = 1 / (float)fps;

  int useDetectionEach = 10;

  vector<vector<PositionInFrame>> all_detections =
      vector<vector<PositionInFrame>>();
  import_detections(detections_path, all_detections, nb_frames);
  spdlog::debug(std::to_string(all_detections.size()) +
                " detections imported !");

  std::string videoPath = "/home/user/got_cpp/data/islanp_recording.mp4";
  std::string outputPath = "/home/user/got_cpp/data/output_video.mp4";
  cv::VideoCapture cap(videoPath);
  cv::VideoWriter writer;
  int ex = static_cast<int>(cap.get(cv::CAP_PROP_FOURCC));
  writer.open(outputPath, ex, fps, cv::Size(width, height));
  cv::Mat frame;

  if (!cap.isOpened()) {
    spdlog::error("Error opening video file " + videoPath);
    return -1;
  }
  bool found = false; // when we dont have detection at first frame
  for (int i = 0; i < nb_frames; i++) {
    spdlog::info(i);
    cap >> frame;

    assert(all_detections.at(i).size() < 2);
    PositionInFrame detection;

    if (!all_detections.at(i).empty() && !found) {
      detection = all_detections.at(i).at(0);
      spdlog::info("initializing..");
      tk.Init(detection);
      found = true;
    } else if(found) {
      //tk.CorrectTime(dT);
      PositionInFrame prediction = tk.Predict(dT);
      // show predicted bbox in here
      cv::rectangle(frame, prediction.toRect(), cv::Scalar(0, 255, 0), 1, 8, 0);
      displayFrameNb(frame, i + 1);
      writer.write(frame);

      if (!all_detections.at(i).empty() && (i % useDetectionEach == 0 || i < 60)) {
        detection = all_detections.at(i).at(0);
        tk.Update(detection);
      }
    }
    else {

    }
  }
  writer.release();
  cap.release();

  spdlog::info("tracking DONE !");
  return 0;
}

int main_linear() {
  spdlog::set_level(spdlog::level::debug);

  TrackerConfig config = TrackerConfig();
  KalmanLinearConstantVelocity tk =
      KalmanLinearConstantVelocity(config.getCovParamsValues());

  spdlog::info("importing detections");
  std::string detections_path = "/home/user/got_cpp/data/islanp_face_moving/";
  int nb_frames = 462;

  int width = 1280;
  int height = 720;
  int fps = 25;
  double dT = 1 / (float)fps;
  int useDetectionEach = 10;


  vector<vector<PositionInFrame>> all_detections =
      vector<vector<PositionInFrame>>();
  import_detections(detections_path, all_detections, nb_frames);
  spdlog::debug(std::to_string(all_detections.size()) +
                " detections imported !");

  std::string videoPath = "/home/user/got_cpp/data/islanp_recording.mp4";
  std::string outputPath = "/home/user/got_cpp/data/output_video.mp4";
  cv::VideoCapture cap(videoPath);
  cv::VideoWriter writer;
  int ex = static_cast<int>(cap.get(cv::CAP_PROP_FOURCC));
  writer.open(outputPath, ex, fps, cv::Size(width, height));
  cv::Mat frame;

  if (!cap.isOpened()) {
    spdlog::error("Error opening video file " + videoPath);
    return -1;
  }
  bool found = false; // when we dont have detection at first frame
  for (int i = 0; i < nb_frames; i++) {
    cap >> frame;

    assert(all_detections.at(i).size() < 2);
    PositionInFrame detection;

    if (!all_detections.at(i).empty() && !found) {
      detection = all_detections.at(i).at(0);
      spdlog::info("initializing..");
      tk.Init(detection);
      found = true;
    } else if(found) {
      //tk.CorrectTime(dT);
      PositionInFrame prediction = tk.Predict(dT);
      // show predicted bbox in here
      cv::rectangle(frame, prediction.toRect(), cv::Scalar(0, 255, 0), 1, 8, 0);
      displayFrameNb(frame, i + 1);
      writer.write(frame);

      if (!all_detections.at(i).empty() && (i % useDetectionEach == 0 || i < 60)) {
        detection = all_detections.at(i).at(0);
        tk.Update(detection);
      }
    }
    else {

    }
  }
  writer.release();
  cap.release();

  spdlog::info("tracking DONE !");
  return 0;
}

int main_multi() {
  spdlog::set_level(spdlog::level::debug);

  spdlog::info("importing detections");
  std::string detections_path = "/home/user/got_cpp/data/sample_1_detections/";
  int nb_frames = 1200;

  int width = 1280;
  int height = 720;
  int fps = 30;
  double dT = 1 / (float)fps;
  int useDetectionEach = 10;


  vector<vector<PositionInFrame>> all_detections =
      vector<vector<PositionInFrame>>();
  import_detections(detections_path, all_detections, nb_frames);
  spdlog::debug(std::to_string(all_detections.size()) +
                " detections imported !");

  std::string videoPath = "/home/user/got_cpp/data/sample_1.mov";
  std::string outputPath = "/home/user/got_cpp/data/output_video_multi.mp4";
  cv::VideoCapture cap(videoPath);
  cv::VideoWriter writer;
  int ex = static_cast<int>(cap.get(cv::CAP_PROP_FOURCC));
  writer.open(outputPath, ex, fps, cv::Size(width, height));
  cv::Mat frame;

  if (!cap.isOpened()) {
    spdlog::error("Error opening video file " + videoPath);
    return -1;
  }

  string confPath = "../tracker/config/tracker_conf.ini";
  MultiObjTracker multiObjTracker = MultiObjTracker<KalmanLinearConstantVelocity>(confPath);

  for (int i = 0; i < nb_frames; i++) {
    // spdlog::info(i);
    cap >> frame;

    vector<PositionInFrame> detections;
    PositionsInFrame predictions;

    if (all_detections.at(i).size() > 0 && (i % useDetectionEach == 0 || i < 10)) {
      detections = all_detections.at(i);
      predictions = multiObjTracker.Predict(detections, dT);
      for (auto &detection: detections) {
        cv::rectangle(frame, detection.toRect(), cv::Scalar(255, 0, 0), 1, 8, 0);
        cv::putText(frame, "DETECTION", cv::Point(detection.toRect().x, detection.toRect().y), cv::FONT_HERSHEY_COMPLEX, 1, cv::Scalar(0,190, 0), 2);
      }
    } else {
      // get frame with timestamp
      predictions = multiObjTracker.Predict(vector<PositionInFrame>(), dT);
    }

    // show predicted bbox in here
    for (auto &prediction : predictions) {
      cv::rectangle(frame, prediction.positionInFrame.toRect(), cv::Scalar(0, 255, 0), 1, 8, 0);
      putText(frame, std::to_string(prediction.ID), cv::Point(prediction.positionInFrame.toRect().x, prediction.positionInFrame.toRect().y+prediction.positionInFrame.height()), cv::FONT_HERSHEY_DUPLEX, 1, cv::Scalar(0, 190, 0), 2);
      displayFrameNb(frame, i + 1);
    }

    writer.write(frame);


  }
  writer.release();
  cap.release();
  spdlog::info("tracking DONE !");

  return 0;
}

int main() {
  // return main_ekf();
  // return main_linear();
  return main_multi();
}