import tracking_python
import cv2
import numpy as np

conf_path = "/home/user/got_cpp/tracker/config/ekf_tracker_conf.ini"

video_path = "/home/user/got_cpp/data/islanp_recording.mp4"
output_path = "/home/user/got_cpp/data/output_video_python.mp4"
height = 720
width = 1280
fps = 30
dT = 1/float(fps)

detections_path = "/home/user/got_cpp/data/islanp_face_moving/"
nb_frames = 462
list_detections = list()

print("importing detections...")
list_detections = tracking_python.import_detections(detections_path, list_detections, nb_frames)
print(f"{len(list_detections)} detections imported !")

writer = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc('M','J','P','G'), fps, (width, height))

cap = cv2.VideoCapture(video_path)
# Check if camera opened successfully
if (cap.isOpened()== False):
    print("Error opening video stream or file")

conf = tracking_python.TrackerConfig(conf_path)
tk = tracking_python.KalmanExtendedCRTV(conf.getCovParamsValues())

found = False
# Read until video is completed
i = 0
while(cap.isOpened() and i < 462):
    # Capture frame-by-frame
    print(i)
    ret, frame = cap.read()
    if ret == True:
        if(len(list_detections[i])>0 and not found) :
            detection = list_detections[i][0]
            tk.init(detection)
            found = True
        elif (found):
            prediction = tk.predict(dT)
            cv2.rectangle(frame, prediction.toRect(), (255, 0, 0), 1, 8, 0)
            writer.write(frame)

            if (len(list_detections[i])>0 and (i%3==0 or i<10)):
                detection = list_detections[i][0]
                tk.update(detection)
        else:
            pass
        i+=1
    # Break the loop
    else:
        break

writer.release()
cap.release()
cv2.destroyAllWindows()

