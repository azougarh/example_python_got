cmake_minimum_required(VERSION 3.10)

set(VERSION 0.2)
project(tracking VERSION ${VERSION} LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_COMPILER /usr/bin/gcc-9 CACHE PATH "" FORCE)
set(CMAKE_CXX_COMPILER /usr/bin/g++-9 CACHE PATH "" FORCE)

#boost
find_package(Boost COMPONENTS system program_options REQUIRED)
include_directories("${Boost_INCLUDE_DIRS}")
###################################################################

# OPENCV
set(OCV_VERSION 4.5.0)
set(OpenCV_DIR /installation/OpenCV-master/lib/cmake/opencv4/)
include_directories(/installation/OpenCV-master/include/)
find_package(OpenCV ${OCV_VERSION} REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
###################################################################

# LOGGER
set(LOGGER_VERSION 1.8.0)
set(spdlog_DIR /usr/local/lib/cmake/spdlog/)
find_package(spdlog ${LOGGER_VERSION} REQUIRED)
include_directories(${spdlog_INCLUDE_DIRS})
###################################################################

# Gtest
configure_file(CMakeLists.txt.in googletest-download/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
        RESULT_VARIABLE result
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
    message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(COMMAND ${CMAKE_COMMAND} --build .
        RESULT_VARIABLE result
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
    message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/googletest-src
        ${CMAKE_CURRENT_BINARY_DIR}/googletest-build
        EXCLUDE_FROM_ALL)

include_directories("${gtest_SOURCE_DIR}/include/")
###################################################################


set(MAIN_SOURCES
        tracker/include/KalmanLinearConstantVelocity.h
        tracker/include/GeneralMonoTracker.h
        tracker/include/MTracker.h
        tracker/include/structs.h
        utils/utils.h
        tracker/include/Hungarian.h
        tracker/include/config.h
        tracker/include/KalmanExtendedCTRV.h
        tracker/src/KalmanExtendedCTRV.cpp
        tracker/src/Hungarian.cpp
        tracker/src/KalmanLinearConstantVelocity.cpp
        tracker/src/MTracker.cpp
        tracker/src/config.cpp)

set(TEST_SOURCES
        tests/mono_object.cpp
        tests/hungarian.cpp
        tests/test_jacobian.cpp)

include_directories(./)

# to be able to link correctly with std::experimental
add_compile_options(-fPIC)
add_library(trackingLib SHARED ${MAIN_SOURCES})
target_link_libraries(trackingLib ${OpenCV_LIBS} ${Boost_LIBRARIES})

add_executable(tracker main.cpp ${MAIN_SOURCES})
target_link_libraries(tracker ${OpenCV_LIBS} ${Boost_LIBRARIES})

#add_compile_options(tests PRIVATE --coverage)
list(APPEND CMAKE_EXE_LINKER_FLAGS -fprofile-arcs)
add_executable(tests tests/main.cpp ${TEST_SOURCES} ${MAIN_SOURCES})
target_link_libraries(tests ${OpenCV_LIBS} gtest_main trackingLib ${Boost_LIBRARIES} gcov)

###################################################################
# Doxygen
# first we can indicate the documentation build as an option and set it to ON by default
option(BUILD_DOC "Build documentation" OFF)

# check if Doxygen is installed
find_package(Doxygen REQUIRED)
if (DOXYGEN_FOUND)
    # set input and output files
    set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/docs/Doxyfile.in)
    set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

    # request to configure the file
    configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
    message("Doxygen build started")

    # note the option ALL which allows to build the docs together with the application
    add_custom_target( doc_doxygen ALL
            COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
            COMMENT "Generating API documentation with Doxygen"
            VERBATIM )
else (DOXYGEN_FOUND)
    message("Doxygen need to be installed to generate the doxygen documentation")
endif (DOXYGEN_FOUND)
###################################################################

# Python binding
if(PYTHON_BINDING)
    set(Python_ADDITIONAL_VERSIONS 3.6 3.7)
    find_package(PythonLibs)
    MESSAGE(STATUS "found python : ${PYTHON_VERSION_STRING}, ${PYTHON_LIBRARIES}")
    include_directories(${PYTHON_INCLUDE_DIRS})

    ## pybind11
    set(PYBIND11_CPP_STANDARD -std=c++17)
    IF(NOT EXISTS pybind11)
        execute_process(COMMAND git clone https://github.com/pybind/pybind11.git)
    endif(NOT EXISTS pybind11)
    add_subdirectory(${CMAKE_BINARY_DIR}/pybind11/)
    include_directories(${CMAKE_BINARY_DIR}/pybind11/include/)

    link_libraries(${PYTHON_LIBRARIES})
    pybind11_add_module(tracking_python bind/tracking_python.cpp)
    target_link_libraries(tracking_python PRIVATE trackingLib ${PYTHON_LIBRARIES})
endif(PYTHON_BINDING)
###################################################################