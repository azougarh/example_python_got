//
// Created by azougarh on 09/09/2020.
//

#ifndef TRACKING_KALMANLINEARCONSTANTVELOCITY_H
#define TRACKING_KALMANLINEARCONSTANTVELOCITY_H

#include "GeneralMonoTracker.h"
#include "structs.h"
#include <opencv4/opencv2/video/tracking.hpp>

/*!
 * implementation of kalman linear constant velocity to track bounding boxes
 */
class KalmanLinearConstantVelocity : public BaseTracking {
public:
  /*!
   * constructor
   * @param initMatrices initialization matrices parsed with config class
   */
  KalmanLinearConstantVelocity(std::map<std::string, cv::Mat> initMatrices) {
    tType = TrackerType::KalmanLinear;
    CreateLinear(initMatrices);
    initMatrices["trackerInitialization.errorCovPostInit"].copyTo(errorCovPostInit);
  }
  ~KalmanLinearConstantVelocity() = default;

  /*!
   * predict new state based on time elapsed since last call to update or predict and
   * corrects the unit time of the tracker
   * @param dt time elapsed (s)
   * @return new position in frame
   */
  PositionInFrame Predict(double dt);

  /*!
   * get the corrected state after being updated with a detection,
   * it takes into account past information along with the new detection
   * @return
   */
  PositionInFrame GetCorrectedState();

  // TODO make predict call Init, to call update also with the 1st bbox
  /*!
   * initialize tracker with first bouding box
   * @param first_bbox
   */
  void Init(PositionInFrame first_bbox);

  /*!
   * update the tracker when new detection is available
   * @param bbox
   */
  void Update(PositionInFrame bbox);

  /*!
   * correct internal time unit of the tracker if needed
   * @param dT
   */
  void CorrectTime(double dT);

  PositionInFrame lastPosition = PositionInFrame();
  TrackerType tType;

private:
  void CreateLinear(std::map<std::string, cv::Mat> initMatrices);

  cv::KalmanFilter kf_tracker;

  int numericType = CV_64F;
  int stateSize = 6; // (center_x, center_y, x', y', width, height)
  int measSize = 4;
  int controlSize = 0;

  cv::Mat state = cv::Mat::zeros(stateSize, 1, numericType);
  cv::Mat meas = cv::Mat::zeros(measSize, 1, numericType);

  cv::Mat errorCovPostInit;
};

#endif // TRACKING_KALMANLINEARCONSTANTVELOCITY_H
