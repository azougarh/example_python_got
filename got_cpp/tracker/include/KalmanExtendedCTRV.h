//
// Created by azougarh on 06/10/2020.
//

#ifndef TRACKING_KALMANEXTENDEDCTRV_H
#define TRACKING_KALMANEXTENDEDCTRV_H

#include "GeneralMonoTracker.h"
#include "structs.h"
#include <opencv4/opencv2/opencv.hpp>

/*!
 * implementation of kalman extended tracker with dynamical model of constant
 * turn rate velocity, to track bounding boxes
 */
class KalmanExtendedCTRV : public BaseTracking {
public:
  /*!
   * constructor
   * @param initMatrices initialization matrices parsed with config
   */
  KalmanExtendedCTRV(std::map<std::string, cv::Mat> initMatrices) {
    tType = TrackerType::KalmanExtended;
    CreateFilter(initMatrices);
    initMatrices["trackerInitialization.errorCovPostInit"].copyTo(errorCovPostInit);
  };
  ~KalmanExtendedCTRV() = default;

  /*!
   * (transfer function) non linear function that links state at time t+1 with state at time t
   * @param statePost state a posteriori
   * @param dt elapsed time
   * @param control optional
   * @return new state
   */
  cv::Mat f(cv::Mat statePost, double dt, cv::Mat control = cv::Mat::zeros(stateSize, stateSize, numericType)); // transfer function

  /*!
   * (measurement function) non linear function used to adapt measurement format from sensor with
   * the state definition
   * @param statePre predicted state
   * @return corrected state
   */
  cv::Mat h(cv::Mat statePre); // measurement correction function

  /*!
   * predict new state position based on the elapsed time
   * corrects the unit time of the tracker
   * @param dt elapsed time (s). default to 0.033 (ie. 30 fps)
   * @return predicted position in frame
   */
  PositionInFrame Predict(double dt = 0.033);

  /*!
   * update when new detection is available
   * @param bbox
   */
  void Update(PositionInFrame bbox);

  /*!
   * initialize the tracker with the first detection
   * @param first_bbox
   */
  void Init(PositionInFrame first_bbox);

  /*!
   * get the corrected state after being updated with a detection,
   * it takes into account past information along with the new detection
   * @return position in frame
   */
  PositionInFrame GetCorrectedState();

  PositionInFrame lastPosition = PositionInFrame();
  TrackerType tType;

  /*!
   * get the jacobian (for test purpose)
   * @return
   */
  const cv::Mat &getJacobianF() const;
  void UpdateJacobianF(cv::Mat state, double dt);

private:
  void CreateFilter(std::map<std::string, cv::Mat> initMatrices);
  void UpdateJacobianH();
  cv::KalmanFilter kf_tracker;

  static const int numericType = CV_64F;
  static const int stateSize = 7; // (center_x, center_y, theta, v, omega, width, height)
  int measSize = 4;
  int controlSize = 0;

  cv::Mat state = cv::Mat::zeros(stateSize, 1, numericType);
  cv::Mat meas = cv::Mat::zeros(measSize, 1, numericType);
  cv::Mat jacobianF = cv::Mat::zeros(stateSize, stateSize, numericType);
  cv::Mat jacobianH = cv::Mat::zeros(stateSize, stateSize, numericType);

  cv::Mat errorCovPostInit;// = cv::Mat::eye(stateSize, stateSize, numericType) * 200.0;

  double tolerance = 1e-6;
};

#endif // TRACKING_KALMANEXTENDEDCTRV_H
