//
// Created by azougarh on 28/09/2020.
//
#include <boost/program_options.hpp>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <spdlog/spdlog.h>
#include <string>
#include <opencv4/opencv2/opencv.hpp>

#define MAX_PATH_LENGTH 1000
using namespace std;
namespace po = boost::program_options;

class TrackerConfig {
public:
  /*!
   *
   * @param confPath
   */
  TrackerConfig(std::string confPath = "../tracker/config/tracker_conf.ini") {
    getConfig(confPath);
  }
  virtual ~TrackerConfig() {}

  /*!
   *
   * @return
   */
  const map<string, cv::Mat> &getCovParamsValues() const;
private:
  void loadConfigContent();
  cv::Mat parseDataFile(std::string path);
  void parseConfigFile(std::string configPath);

  vector<string> paramsPaths{      // params whom values are paths that need to be read
      "trackerInitialization.processNoiseCov",
      "trackerInitialization.measurementNoiseCov",
      "trackerInitialization.errorCovPostInit"
  };

  vector<string> params{         // params that are read directly
      "root.path",
      "video.fps"
  };

  void getConfig(const std::string& confPath = "../tracker/config/tracker_conf.ini");

  map<string, string> paramsInfos;       // contains covariance files paths
  map<string, cv::Mat> covParamsValues;

  // contains params with covariances matrices
  map<string, string> paramsValues;      // contains atomic values (as string, to cast to a specific type)

  static const int numericType = CV_64F;
};