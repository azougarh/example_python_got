//
// Created by azougarh on 18/09/2020.
//

#ifndef TRACKING_GENERALMONOTRACKER_H
#define TRACKING_GENERALMONOTRACKER_H

#include "structs.h"
#include <math.h>
#include <opencv4/opencv2/opencv.hpp>

class BaseTracking {};

// a layer that handles the type of kalman used, IDs, distances
// and criteria of killing tracks
/*!
 * class that takes as a template param the type of tracker (inherited from BaseTracking)
 * @tparam T
 */
template <class T>
class GeneralMonoTracker {
public:
  /*!
   * Constructor
   * @param ID for the track
   * @param initMatrices initialization matrices for kalman filter
   */
  GeneralMonoTracker(int ID, std::map<std::string, cv::Mat> initMatrices)
      : trackedID(ID), skippedFrames(0), tracker(initMatrices),
        tType(tracker.tType) {}

  /*!
   *
   * @param dt elapsed time since latest call of predict or update
   * @return bounding box of the tracked subject
   */
  PositionInFrame Predict(double dt) {
    lastPosition = tracker.Predict(dt);
    return lastPosition;
  }

  /*!
   * initialize the tracker using the first bounding box
   * @param first_bbox
   */
  void Init(PositionInFrame first_bbox) { tracker.Init(first_bbox); }

  /*!
   * update when new detection is available
   * @param bbox
   */
  void Update(PositionInFrame bbox) {
    tracker.Update(bbox);
    lastPosition = tracker.lastPosition;
    skippedFrames = 0;
  }

  /*!
   * update time unit of the tracker if needed
   * @param dT
   */
  void CorrectTime(double dT) { tracker.CorrectTime(dT); }

  /*!
   * criteria to kill a track
   * @param maxSkippedFrames
   * @return true to be killed otherwise false
   */
  bool ToKill(int maxSkippedFrames) {
    return (skippedFrames > maxSkippedFrames);
  }

  /*!
   * compute distance to an other bounding box
   * @param other
   * @return distance value (pixels)
   */
  double DistanceTo(PositionInFrame other);

  /*!
   *
   */
  void SkippedFrame() {
    skippedFrames++;
    return;
  }

  /*!
   * getter of the latest position of the track
   * @return
   */
  const PositionInFrame &getLastPosition() const;

private:
  T tracker;
  int trackedID;

public:
  /*!
   *
   * @return
   */
  int getTrackedId() const;

private:
  // TODO initialize ID
  int skippedFrames;

  PositionInFrame lastPosition;
  TrackerType tType;
};

template <class T>
double GeneralMonoTracker<T>::DistanceTo(PositionInFrame other) {
  return sqrt(pow(lastPosition.centerX() - other.centerX(), 2) +
              pow(lastPosition.centerY() - other.centerY(), 2));
}

template <class T>
const PositionInFrame &GeneralMonoTracker<T>::getLastPosition() const {
  return lastPosition;
}
template <class T> int GeneralMonoTracker<T>::getTrackedId() const {
  return trackedID;
}

#endif // TRACKING_GENERALMONOTRACKER_H
