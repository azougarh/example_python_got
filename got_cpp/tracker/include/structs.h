//
// Created by azougarh on 09/09/2020.
//

#ifndef TRACKING_STRUCTS_H
#define TRACKING_STRUCTS_H

#include <opencv4/opencv2/core/types.hpp>

/*!
 * class that embed a bouding box
 */
class PositionInFrame {
public:
  /*!
   * empty constructor
   */
  PositionInFrame() : _cx(0), _cy(0), _w(0), _h(0){};

  /*!
   * constructor
   * @param x X of center of the bounding box
   * @param y Y of center of the bounding box
   * @param w width of the bounding box
   * @param h height of the bouding box
   */
  PositionInFrame(int x, int y, int w, int h) : _cx(x), _cy(y), _w(w), _h(h){};

  /*!
   * constructor from cv::Rect given bounding box containing top-left coordinates
   * and width/height
   * @param bbox
   */
  PositionInFrame(cv::Rect bbox)
      : // bbox contains (top left + with & height)
        _cx(bbox.x + bbox.width / 2.), _cy(bbox.y + bbox.height / 2.),
        _w(bbox.width), _h(bbox.height){};

  /*!
   * get the X coordinate center of the bounding box
   * @return
   */
  double centerX() { return _cx; }

  /*!
   * get the Y coordinate of the bouding box
   * @return
   */
  double centerY() { return _cy; }

  /*!
   * get width of the bounding box
   * @return
   */
  int width() { return _w; }

  /*!
   * get the height of the bounding box
   * @return
   */
  int height() { return _h; }

  /*!
   * get the bounding box as a cv::Rect strcuture
   * @return
   */
  cv::Rect toRect() { return cv::Rect(topLeftX(), topLeftY(), _w, _h); } ;

  /*!
   * if the bbox if empty
   * @return
   */
  bool isEmpty() {
    return (_cx==0) && (_cy==0) && (_w==0) && (_h==0);
  }

private:
  double _cx; // centerX center of the box
  double _cy; // y center of the box
  int _w;  // width of the box
  int _h;  // height of the box

  int topLeftX() { return static_cast<int>(_cx - _w / 2.); }
  int topLeftY() { return static_cast<int>(_cy - _h / 2.); }
};

/*!
 * internal struct to enumerate type of trackers used
 */
enum TrackerType {
  KalmanLinear,
  KalmanExtended
};

/*!
 * internal struct to enumerate available distances
 * for now only distCenter is supported
 */
enum DistType {
  DistCenters,  // Euclidean distance between centers, pixels
  DistRects,    // Euclidean distance between bounding rectangles, pixels
  DistJaccard,  // Intersection over Union, IoU, [0, 1]
  DistHist,     // Bhatacharia distance between histograms, [0, 1]
  DistsCount
};


#endif // TRACKING_STRUCTS_H
