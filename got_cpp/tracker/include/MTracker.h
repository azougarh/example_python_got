//
// Created by azougarh on 18/09/2020.
//

#ifndef TRACKING_MTRACKER_H
#define TRACKING_MTRACKER_H

#include "GeneralMonoTracker.h"
#include "KalmanLinearConstantVelocity.h"
#include "KalmanExtendedCTRV.h"
#include "Hungarian.h"
#include "config.h"
#include "structs.h"
#include <opencv4/opencv2/core.hpp>
#include <vector>

namespace MTracker {
/*!
 * structure that embed position of a subject in a frame using a bounding box
 */
struct PositionInFrameID {
  int ID;
  PositionInFrame positionInFrame;
};
typedef std::vector<PositionInFrameID> PositionsInFrame;
typedef std::tuple<cv::Mat, double> ImageTS; // TODO change struct

/*!
 * a layer that handles assignment, returning the results and manages tracks
 * templated with the type of tracker used
 * @tparam T type of the used tracker (child class of BaseTracking)
 */
template <class T>
class MultiObjTracker {
public:
  /*!
   * empty constructor without configuration
   */
  MultiObjTracker() {}

  /*!
   * constructor using configuration
   * @param configPath
   */
  MultiObjTracker(std::string configPath = "../tracker/config/tracker_conf.ini")
      : trackerConfig(configPath) {
    if (std::is_same<T, KalmanLinearConstantVelocity>::value)
      tType = TrackerType::KalmanLinear;
    if (std::is_same<T, KalmanExtendedCTRV>::value)
      tType = TrackerType::KalmanExtended;
  }

  virtual ~MultiObjTracker() {}

  /*!
   * predict object position based on elapsed time.
   * if detections is not empty then they are used to update the trackers
   * @param detections new detections if available or empty
   * @param dT elapsed time (s)
   * @return return predicted positions of subjects
   */
  PositionsInFrame Predict(const vector<PositionInFrame>& detections, double dT);

private:
  void CreateCostMatrix(std::vector<std::vector<double>> &costMatrix,
                        const vector<PositionInFrame> &detections);
  void CreateTrack();

  double maxCost = 1000.;
  double distanceThreshold =
      10000; //~ in pixels (should be function of fps, resolution, speed..)
  int maximumSkippedFrames = 45; // should be function of fps and not constant !
  int lastID = 0;

  TrackerType tType = TrackerType::KalmanLinear;

  HungarianAlgorithm hungarian;

  TrackerConfig trackerConfig;


  typedef GeneralMonoTracker<T> Tracker;

  typedef std::unique_ptr<Tracker> Track;
  typedef std::vector<Track> Tracks; // just plural of the above

  Tracks tracks = Tracks();
};
} // namespace MTracker
#endif // TRACKING_MTRACKER_H
