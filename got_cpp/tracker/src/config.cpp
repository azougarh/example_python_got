//
// Created by azougarh on 28/09/2020.
//
#include "tracker/include/config.h"
#include <iostream>


void TrackerConfig::getConfig(const std::string& confPath) {
  parseConfigFile(confPath);
  loadConfigContent();
}

// method that reads content of ini file, and fills paramsInfos map
void TrackerConfig::parseConfigFile(std::string configPath) {
  char absolutePath[MAX_PATH_LENGTH];
  realpath(configPath.c_str(), absolutePath);
  spdlog::info("reading conf from {}", absolutePath);

  std::ifstream ini_file(absolutePath);

  po::options_description configFileOptions;

  for (string &param: paramsPaths){
    const char *paramChar = param.c_str();
    configFileOptions.add_options()(paramChar, po::value<string>()->composing(), paramChar);
  }
  for (string &param: params){
    const char *paramChar = param.c_str();
    configFileOptions.add_options()(paramChar, po::value<string>()->composing(), paramChar);
  }

  po::variables_map vm;

  po::store(po::parse_config_file(ini_file, configFileOptions), vm);
  po::notify(vm);

  for (string &param: paramsPaths){
    if (vm.count(param)) {
      paramsInfos[param] = vm[param].as<string>();
    } else {
      spdlog::error("parameter {0} not found in config file {1}", param, absolutePath);
    }
  }
  for (string &param: params){
    if (vm.count(param)) {
      paramsValues[param] = vm[param].as<string>();
    } else {
      spdlog::error("parameter {0} not found in config file {1}", param, absolutePath);
    }
  }
}

// method that uses config infos to read content of related csv files
void TrackerConfig::loadConfigContent() {
  for (string &param: paramsPaths) {
    cv::Mat value = parseDataFile(paramsValues["root.path"] + paramsInfos[param]);
    covParamsValues[param] = value;
  }
}

cv::Mat TrackerConfig::parseDataFile(std::string path) {
  // source; https://waterprogramming.wordpress.com/2017/08/20/reading-csv-files-in-c/
  // Parses a csv file into a vector<vector<double>>
  vector<vector<double>> data;
  ifstream inputFile(path);
  int l = 0;

  while (inputFile) {
    l++;
    string s;
    if (!getline(inputFile, s)) break;
    if (s[0] != '#') {
      istringstream ss(s);
      vector<double> record;

      while (ss) {
        string line;
        if (!getline(ss, line, ','))
          break;
        try {
          record.push_back(stof(line));
        }
        catch (const std::invalid_argument& e) {
          spdlog::info("NaN found in file {0} line {1}", path, l);
          e.what();
        }
      }

      data.push_back(record);
    }
  }

  if (!inputFile.eof()) {
    spdlog::error("Could not read file {}", path);
    __throw_invalid_argument("File not found.");
  }

  cv::Mat dataMat(data.size(), data.at(0).size(), numericType);
  for (int i=0; i<dataMat.rows; i++) {
    for (int j=0; j<dataMat.cols; j++) {
      dataMat.at<double>(i, j) = data.at(i).at(j);
    }
  }
  return dataMat;
}
const map<string, cv::Mat> &TrackerConfig::getCovParamsValues() const {
  return covParamsValues;
}
