//
// Created by azougarh on 06/10/2020.
//
#include "tracker/include/KalmanExtendedCTRV.h"
#include <math.h>
#include <spdlog/spdlog.h>

using namespace std;

cv::Mat KalmanExtendedCTRV::f(cv::Mat statePost, double dt, cv::Mat control) {
  double x = statePost.at<double>(0);
  double y = statePost.at<double>(1);
  double theta = statePost.at<double>(2);
  double v = statePost.at<double>(3);
  double omega = statePost.at<double>(4);
  double w = statePost.at<double>(5);
  double h = statePost.at<double>(6);

  cv::Mat newState = cv::Mat::zeros(stateSize, 1, numericType);

  if (abs(omega) < tolerance) {
    newState.at<double>(0) = v * dt * cos(theta) + x;
    newState.at<double>(1) = v * dt * sin(theta) + y;
  } else {
    newState.at<double>(0) =
        v / omega * sin(omega * dt + theta) - v / omega * sin(theta) + x;
    newState.at<double>(1) =
        -v / omega * cos(omega * dt + theta) + v / omega * cos(theta) + y;
  }

  newState.at<double>(2) = omega * dt + theta;
  newState.at<double>(3) = v;
  newState.at<double>(4) = omega;
  newState.at<double>(5) = w;
  newState.at<double>(6) = h;
  return newState;
}

cv::Mat KalmanExtendedCTRV::h(cv::Mat statePre) {
  cv::Mat predicted = cv::Mat::zeros(measSize, 1, numericType);
  predicted.at<double>(0) = statePre.at<double>(0);
  predicted.at<double>(1) = statePre.at<double>(1);
  predicted.at<double>(2) = statePre.at<double>(5);
  predicted.at<double>(3) = statePre.at<double>(6);
  return predicted;
}

void KalmanExtendedCTRV::CreateFilter(std::map<std::string, cv::Mat> initMatrices) {
  cv::KalmanFilter kf(stateSize, measSize, controlSize, numericType);

  kf.measurementMatrix = cv::Mat::zeros(measSize, stateSize, numericType);
  kf.measurementMatrix.at<double>(0, 0) = 1.0f;
  kf.measurementMatrix.at<double>(1, 1) = 1.0f;
  kf.measurementMatrix.at<double>(2, 5) = 1.0f;
  kf.measurementMatrix.at<double>(3, 6) = 1.0f;

  initMatrices["trackerInitialization.processNoiseCov"].copyTo(kf.processNoiseCov);

  initMatrices["trackerInitialization.measurementNoiseCov"].copyTo(kf.measurementNoiseCov);

  kf_tracker = kf;
}

PositionInFrame KalmanExtendedCTRV::Predict(double dt) {
  cv::Mat temp = kf_tracker.statePost;

  spdlog::debug("updating jacobian");
  UpdateJacobianF(state, dt);
  kf_tracker.transitionMatrix = jacobianF;

  cv::Mat h_statPre = h(kf_tracker.statePre);
  spdlog::debug("state_Pre in predict 1: {0} {1} {2} {3}",
                h_statPre.at<double>(0), h_statPre.at<double>(1),
                h_statPre.at<double>(2), h_statPre.at<double>(3));

  spdlog::debug("predicting state");
  state = kf_tracker.predict();
  spdlog::debug("state_Pre in predict 2: {0} {1} {2} {3} (dt = {4})", h_statPre.at<double>(0),
                h_statPre.at<double>(1), h_statPre.at<double>(2),
                h_statPre.at<double>(3), dt);
  kf_tracker.statePre = f(temp, dt);

  h_statPre = h(kf_tracker.statePre);
  spdlog::debug("state_Pre in predict: {0} {1} {2} {3}", h_statPre.at<double>(0),
                h_statPre.at<double>(1), h_statPre.at<double>(2),
                h_statPre.at<double>(3));

  spdlog::debug("getting prediction");
  state = f(state, dt);
  int width = state.at<double>(5);
  int height = state.at<double>(6);
  int cx = (int)state.at<double>(0);
  int cy = (int)state.at<double>(1);

  lastPosition = PositionInFrame(cx, cy, width, height);

  spdlog::debug("predicting: cx={0}, cy={1}, width={2}, height={3}", cx, cy,
                width, height);
  return {cx, cy, width, height};
}

void KalmanExtendedCTRV::Update(PositionInFrame bbox) {
  // TODO update jacobian before using it
  // kf_tracker.measurementMatrix = jacobianH;

  meas.at<double>(0) = bbox.centerX();
  meas.at<double>(1) = bbox.centerY();
  meas.at<double>(2) = (double)bbox.width();
  meas.at<double>(3) = (double)bbox.height();

  kf_tracker.correct(meas);

  cv::Mat h_statPre = h(kf_tracker.statePre);
  spdlog::debug("state_Pre in update: {0} {1} {2} {3}", h_statPre.at<double>(0),
                h_statPre.at<double>(1), h_statPre.at<double>(2),
                h_statPre.at<double>(3));

  kf_tracker.temp5 = meas - h(kf_tracker.statePre);
  kf_tracker.statePost =
      kf_tracker.statePre + kf_tracker.gain * kf_tracker.temp5;

  state = kf_tracker.statePost;
  lastPosition = GetCorrectedState();
}

PositionInFrame KalmanExtendedCTRV::GetCorrectedState() {
  int width = kf_tracker.statePost.at<double>(5);
  int height = kf_tracker.statePost.at<double>(6);
  int cx = (int)kf_tracker.statePost.at<double>(0);
  int cy = (int)kf_tracker.statePost.at<double>(1);

  return {cx, cy, width, height};
}

void KalmanExtendedCTRV::Init(PositionInFrame first_bbox) {
  spdlog::debug("initializing ekf with 1st bbox");
  errorCovPostInit.copyTo(kf_tracker.errorCovPost);

  Update(first_bbox);

  state.at<double>(0) = meas.at<double>(0); // x
  state.at<double>(1) = meas.at<double>(1); // y
  state.at<double>(2) = 0; // atan(meas.at<double>(1)/meas.at<double>(0)); //theta
  state.at<double>(3) = 0; // v
  state.at<double>(4) = 0; // omega
  state.at<double>(5) = meas.at<double>(2); // w
  state.at<double>(6) = meas.at<double>(3); // h

  kf_tracker.statePost = state;
  lastPosition = GetCorrectedState();
  spdlog::debug("lastPosition : cx={0}, cy={1}, w={2}, h={3}",
                meas.at<double>(0), meas.at<double>(1), meas.at<double>(2),
                meas.at<double>(3));
}

void KalmanExtendedCTRV::UpdateJacobianH() {}

void KalmanExtendedCTRV::UpdateJacobianF(cv::Mat previousState, double dt) {
  double theta = previousState.at<double>(2);
  double v = previousState.at<double>(3);
  double omega = previousState.at<double>(4);

  jacobianF.at<double>(0, 0) = 1;
  jacobianF.at<double>(0, 1) = 0;
  if (abs(omega) > tolerance) {
    jacobianF.at<double>(0, 2) =
        v / omega * cos(omega * dt + theta) - v / omega * cos(theta);
    jacobianF.at<double>(0, 3) =
        1 / omega * sin(omega * dt + theta) - 1 / omega * sin(theta);
    jacobianF.at<double>(0, 4) = v * dt * cos(omega * dt + theta) / omega -
                                v / pow(omega, 2) * sin(omega * dt + theta) +
                                v / pow(omega, 2) * sin(theta); // df/d(omega)
  } else {
    jacobianF.at<double>(0, 2) = -v * dt * sin(theta);
    jacobianF.at<double>(0, 3) = dt * cos(theta);
    jacobianF.at<double>(0, 4) = -v * pow(dt, 2) * sin(theta) / 2;
  }
  jacobianF.at<double>(0, 5) = 0;
  jacobianF.at<double>(0, 6) = 0;

  jacobianF.at<double>(1, 0) = 0;
  jacobianF.at<double>(1, 1) = 1;
  if (abs(omega) > tolerance) {
    jacobianF.at<double>(1, 2) =
        v / omega * sin(omega * dt + theta) - v / omega * sin(theta);
    jacobianF.at<double>(1, 3) =
        -1 / omega * cos(omega * dt + theta) + 1 / omega * cos(theta);
    jacobianF.at<double>(1, 4) =
        v * dt * sin(omega * dt + theta) / omega +
        v / pow(omega, 2) *
            (cos(omega * dt + theta - cos(theta))); // df/d(omega)
  } else {
    jacobianF.at<double>(1, 2) = v * dt * cos(theta);
    jacobianF.at<double>(1, 3) = dt * sin(theta);
    jacobianF.at<double>(1, 4) = v * pow(dt, 2) * cos(theta) / 2;
  }
  jacobianF.at<double>(1, 5) = 0;
  jacobianF.at<double>(1, 6) = 0;

  jacobianF.at<double>(2, 0) = 0;
  jacobianF.at<double>(2, 1) = 0;
  jacobianF.at<double>(2, 2) = 1;
  jacobianF.at<double>(2, 3) = 0;
  jacobianF.at<double>(2, 4) = dt;
  jacobianF.at<double>(2, 5) = 0;
  jacobianF.at<double>(2, 6) = 0;

  jacobianF.at<double>(3, 0) = 0;
  jacobianF.at<double>(3, 1) = 0;
  jacobianF.at<double>(3, 2) = 0;
  jacobianF.at<double>(3, 3) = 1;
  jacobianF.at<double>(3, 4) = 0;
  jacobianF.at<double>(3, 5) = 0;
  jacobianF.at<double>(3, 6) = 0;

  jacobianF.at<double>(4, 0) = 0;
  jacobianF.at<double>(4, 1) = 0;
  jacobianF.at<double>(4, 2) = 0;
  jacobianF.at<double>(4, 3) = 0;
  jacobianF.at<double>(4, 4) = 1;
  jacobianF.at<double>(4, 5) = 0;
  jacobianF.at<double>(4, 6) = 0;

  jacobianF.at<double>(5, 0) = 0;
  jacobianF.at<double>(5, 1) = 0;
  jacobianF.at<double>(5, 2) = 0;
  jacobianF.at<double>(5, 3) = 0;
  jacobianF.at<double>(5, 4) = 0;
  jacobianF.at<double>(5, 5) = 1;
  jacobianF.at<double>(5, 6) = 0;

  jacobianF.at<double>(6, 0) = 0;
  jacobianF.at<double>(6, 1) = 0;
  jacobianF.at<double>(6, 2) = 0;
  jacobianF.at<double>(6, 3) = 0;
  jacobianF.at<double>(6, 4) = 0;
  jacobianF.at<double>(6, 5) = 0;
  jacobianF.at<double>(6, 6) = 1;
}
const cv::Mat &KalmanExtendedCTRV::getJacobianF() const { return jacobianF; }
