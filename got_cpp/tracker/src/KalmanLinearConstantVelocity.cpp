//
// Created by azougarh on 09/09/2020.
//

#include "tracker//include/KalmanLinearConstantVelocity.h"
#include <opencv4/opencv2/video/tracking.hpp>
#include <spdlog/spdlog.h>

void KalmanLinearConstantVelocity::CreateLinear(std::map<std::string, cv::Mat> initMatrices) {
  cv::KalmanFilter kf(stateSize, measSize, controlSize, numericType);

  kf.measurementMatrix = cv::Mat::zeros(measSize, stateSize, numericType);
  kf.measurementMatrix.at<double>(0) = 1.0f;
  kf.measurementMatrix.at<double>(7) = 1.0f;
  kf.measurementMatrix.at<double>(16) = 1.0f;
  kf.measurementMatrix.at<double>(23) = 1.0f;

  cv::setIdentity(kf.transitionMatrix);

  initMatrices["trackerInitialization.processNoiseCov"].copyTo(kf.processNoiseCov);

  initMatrices["trackerInitialization.measurementNoiseCov"].copyTo(kf.measurementNoiseCov);

  kf_tracker = kf;
}

// TODO pass DT to predict and call this method internally from predict()
// TODO correct also covariance matrix using dT
void KalmanLinearConstantVelocity::CorrectTime(double dT) {
  kf_tracker.transitionMatrix.at<double>(2) = dT;
  kf_tracker.transitionMatrix.at<double>(9) = dT;
  // spdlog::debug("time unit set to" + std::to_string(dT));
}

PositionInFrame KalmanLinearConstantVelocity::GetCorrectedState() {
  int width = kf_tracker.statePost.at<double>(4);
  int height = kf_tracker.statePost.at<double>(5);
  int cx = (int)kf_tracker.statePost.at<double>(0);
  int cy = (int)kf_tracker.statePost.at<double>(1);

  return {cx, cy, width, height};
}

PositionInFrame KalmanLinearConstantVelocity::Predict(double dt) {
  CorrectTime(dt);
  state = kf_tracker.predict();

  int width = state.at<double>(4);
  int height = state.at<double>(5);
  int cx = (int)state.at<double>(0);
  int cy = (int)state.at<double>(1);

  lastPosition = PositionInFrame(cx, cy, width, height);

  return {cx, cy, width, height};
}

// TODO allow covariance measurement at each update
void KalmanLinearConstantVelocity::Update(PositionInFrame bbox) {
  meas.at<double>(0) = bbox.centerX();
  meas.at<double>(1) = bbox.centerY();
  meas.at<double>(2) = (double)bbox.width();
  meas.at<double>(3) = (double)bbox.height();
  kf_tracker.correct(meas);

  lastPosition = GetCorrectedState();
  // Probably would be better to return statePost content (instead of statePre)
  // when doing the update in order to take into account all the information
  // that we have so far
  // TODO check this
}

// TODO pass cov matrices as parameter to Init (with image or image dimensions)
void KalmanLinearConstantVelocity::Init(PositionInFrame first_bbox) {

  /*// TODO edit indexing access to (i,j)
  kf_tracker.errorCovPre.at<double>(0) = 10;
  kf_tracker.errorCovPre.at<double>(7) = 10;
  kf_tracker.errorCovPre.at<double>(14) = 10;
  kf_tracker.errorCovPre.at<double>(21) = 10;
  kf_tracker.errorCovPre.at<double>(28) = 10;
  kf_tracker.errorCovPre.at<double>(35) = 10;
  */
  kf_tracker.errorCovPost = errorCovPostInit.clone();

  Update(first_bbox);

  state.at<double>(0) = meas.at<double>(0);
  state.at<double>(1) = meas.at<double>(1);
  state.at<double>(2) = 0;
  state.at<double>(3) = 0;
  state.at<double>(4) = meas.at<double>(2);
  state.at<double>(5) = meas.at<double>(3);

  kf_tracker.statePost = state;
  lastPosition = GetCorrectedState();
}
