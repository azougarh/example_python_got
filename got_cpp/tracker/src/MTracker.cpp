//
// Created by azougarh on 21/09/2020.
//

#include "tracker/include/MTracker.h"
#include <spdlog/spdlog.h>

using namespace std;

template <class T>
MTracker::PositionsInFrame MTracker::MultiObjTracker<T>::Predict(const vector<PositionInFrame>& detections, double dT) {

  const int N = detections.size();
  const int M = tracks.size();

  vector<int> assignments(N, -1);  // assign detections -> tracks
  vector<vector<double>> costMatrix(N, vector<double>(M, maxCost));

  // make prediction for all tracks at the present moment
  spdlog::debug("calling predict for each track");
  for (Track &track : tracks) {
    //track->CorrectTime(dT);
    track->Predict(dT);
  }

  // make assignments of new detections to tracks
  if (!tracks.empty() && !detections.empty()) {
    // create cost matrix
    CreateCostMatrix(costMatrix, detections);

    // call assignment
    // fill assignments vector
    spdlog::debug("calling hungarian solver");
    hungarian.Solve(costMatrix, assignments);
    spdlog::debug("dist_matrix; Nrows = {0}, Ncols = {1}", costMatrix.size(), costMatrix[0].size());

    spdlog::debug("assignment done;");
    for (int i=0; i<assignments.size(); i++)
      spdlog::debug("assigning {0} to {1}", i, assignments[i]);
  }

  // un-assign when distance is too large
  spdlog::debug("unassign for large distances");
  for (int j=0; j<tracks.size(); j++){
    for (int i=0; i<detections.size(); i++) {
      if (costMatrix[i][j] > distanceThreshold) {
        spdlog::debug("unassign {0}->{1}, because cost is {2}", i, assignments[i], costMatrix[i][j]);
        assignments[i] = -1;
      }
    }
  }

  // start new tracks for non assigned detections
  spdlog::debug("start new tracks for new detections");
  if (!detections.empty()) {
    for (int i = 0; i < assignments.size(); i++) {
      if (assignments[i] == -1) {
        spdlog::debug("creating a new track for detection {}", i);
        CreateTrack();
        tracks[i]->Init(detections[i]);
      }
    }
  }

  // update filters and get states
  spdlog::debug("updating tracks with new detections");
  for (int i=0; i<tracks.size(); i++) {
    auto it = std::find(assignments.begin(), assignments.end(), i);
    if ( it == assignments.end()) {
      //tracks[i]->Predict();
      tracks[i]->SkippedFrame();
    }
    else {
      int idx = std::distance(assignments.begin(), it);
      tracks[i]->Update(detections[idx]);
    }
  }

  spdlog::debug("extracting new predictions");
  PositionsInFrame results;
  for (auto &track: tracks) {
    PositionInFrameID positionID = {track->getTrackedId(),
                                    track->getLastPosition()};
    results.push_back(positionID);
  }

  // kill non updated tracks
  spdlog::debug("kill outdated tracks");
  for (int i = 0; i < tracks.size(); i++) {
    if (tracks.at(i)->ToKill(maximumSkippedFrames)) {
      tracks.erase(tracks.begin() + i);
    }
  }

  spdlog::debug("prediction done !");
  return results;
}

template <class T>
void MTracker::MultiObjTracker<T>::CreateCostMatrix(
    vector<vector<double>> &costMatrix,
    const vector<PositionInFrame>& detections) {
  spdlog::debug("creating cost matrix");
  for (int j = 0; j < tracks.size(); j++) {
    for (int i = 0; i < detections.size(); i++) {
      auto distance = tracks[j]->DistanceTo(detections[i]);
      if (distance < maxCost)
        costMatrix[i][j] = distance;
      else
        costMatrix[i][j] = maxCost;
    }
  }
}

template <class T>
void MTracker::MultiObjTracker<T>::CreateTrack() {
  Track track;
  lastID++;
  switch (tType) {
  case TrackerType::KalmanLinear:
    track = std::make_unique<Tracker>(lastID, trackerConfig.getCovParamsValues());
    tracks.push_back(std::move(track));
  case TrackerType::KalmanExtended:
    track = std::make_unique<Tracker>(lastID, trackerConfig.getCovParamsValues());
    tracks.push_back(std::move(track));
  default:
    return;
  }
}
