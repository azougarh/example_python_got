//
// Created by azougarh on 22/10/2020.
//

#include "tracker/src/MTracker.cpp"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

// multi-object tracking binding
void mtracker_python(py::module& m) {
  py::class_<MTracker::MultiObjTracker<KalmanLinearConstantVelocity>>(m, "MTrackerLinear")
      .def(py::init<std::string>());

  py::class_<MTracker::MultiObjTracker<KalmanExtendedCTRV>>(m, "MTrackerExtended")
      .def(py::init<std::string>());
}

// if using kalman mono a struct for config would be needed
void config(py::module& m) {
  using namespace pybind11::literals;
  py::class_<TrackerConfig>(m, "TrackerConfig")
      .def(py::init<std::string>(), "conf_path"_a)
      .def("getCovParamsValues", &TrackerConfig::getCovParamsValues);
}

// if necessary to use, mono object tracking (linear)
void kalmanlinear(py::module& m) {
  py::class_<KalmanLinearConstantVelocity>(m, "KalmanLinearCV")
      .def(py::init<std::map<std::string, cv::Mat>>())
      .def("predict", &KalmanLinearConstantVelocity::Predict)
      .def("update", &KalmanLinearConstantVelocity::Update)
      .def("init", &KalmanLinearConstantVelocity::Init);
}

// if necessary to use, mono object tracking (extended)
void kalmanExtended(py::module& m) {
  py::class_<KalmanExtendedCTRV>(m, "KalmanExtendedCRTV")
      .def(py::init<std::map<std::string, cv::Mat>>())
      .def("predict", &KalmanExtendedCTRV::Predict)
      .def("update", &KalmanExtendedCTRV::Update)
      .def("init", &KalmanExtendedCTRV::Init);
}