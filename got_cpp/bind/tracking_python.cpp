//
// Created by azougarh on 22/10/2020.
//

#include "structs_python.h"
#include "mtracker_python.h"
#include "utils_python.h"
#include "opencv_types.h"

#include <pybind11/pybind11.h>
namespace py = pybind11;

PYBIND11_MODULE(tracking_python, m){
  mtracker_python(m);
  structs_python(m);
  config(m);
  kalmanlinear(m);
  kalmanExtended(m);

  utils(m);
}