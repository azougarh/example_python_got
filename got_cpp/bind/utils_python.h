//
// Created by azougarh on 26/10/2020.
//

#ifndef TRACKING_UTILS_PYTHON_H
#define TRACKING_UTILS_PYTHON_H

#include "utils/utils.h"

#include <pybind11/stl.h>
#include <pybind11/pybind11.h>
namespace py = pybind11;

void utils(py::module& m) {
  using namespace pybind11::literals;
  m.def("import_detections", &import_detections, "input"_a, "all_regions"_a, "nb_frames"_a);
}


#endif // TRACKING_UTILS_PYTHON_H
