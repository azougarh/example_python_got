//
// Created by azougarh on 22/10/2020.
//
#include "tracker/include/structs.h"

#include <pybind11/pybind11.h>
namespace py = pybind11;


void structs_python(py::module& m) {
  using namespace pybind11::literals;
  py::class_<PositionInFrame>(m, "PositionInFrame")
      .def(py::init<>())
      .def(py::init<int, int, int, int>(), "x"_a, "y"_a, "w"_a, "h"_a)
      .def(py::init<cv::Rect>(), "bbox"_a)
      .def("centerX", &PositionInFrame::centerX)
      .def("centerY", &PositionInFrame::centerY)
      .def("width", &PositionInFrame::width)
      .def("height", &PositionInFrame::height)
      .def("toRect", &PositionInFrame::toRect)
      .def("isEmpty", &PositionInFrame::isEmpty);
}
