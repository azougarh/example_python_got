//
// Created by azougarh on 14/09/2020.
//
#include "spdlog/spdlog.h"
#include "gtest/gtest.h"
#include <opencv2/opencv.hpp>
#include <vector>
#include <numeric>

#include "tracker/include/GeneralMonoTracker.h"
#include "tracker/include/KalmanExtendedCTRV.h"
#include "tracker/include/KalmanLinearConstantVelocity.h"
#include "tracker/include/config.h"
#include "utils/utils.h"

using namespace std;

int nb_frames = 463; //586;
std::string detectionsPath = "/tmp/data/islanp_face_moving/";
std::string videoPath = "/tmp/data/islanp_recording.mp4";
vector<vector<PositionInFrame>> all_detections =
    vector<vector<PositionInFrame>>();

void setup() {
  if (all_detections.size()==nb_frames)
    return;

  import_detections(detectionsPath, all_detections, nb_frames);
  EXPECT_EQ(all_detections.size(), nb_frames);
}

template <typename T>
void getTrackerPredictions(GeneralMonoTracker<T> tk, string videoPath, string outputPath, int detectionsFrequency,
                vector<vector<PositionInFrame>> &trackerPredictions) {

  // TODO make this function agnostic to the type of kalman used

  EXPECT_EQ(all_detections.size(), nb_frames);
  //TrackerConfig config = TrackerConfig();
  //GeneralMonoTracker tk = GeneralMonoTracker<KalmanLinearConstantVelocity>(0, config.getCovParamsValues());
  //KalmanLinearConstantVelocity tk = KalmanLinearConstantVelocity(config.getCovParamsValues());

  cv::VideoCapture capture(videoPath);
  double fps = capture.get(cv::CAP_PROP_FPS);
  int width = capture.get(cv::CAP_PROP_FRAME_WIDTH);
  int height = capture.get(cv::CAP_PROP_FRAME_HEIGHT);
  double dT = 1 / fps;

  spdlog::debug("fps={0}, width={1}, height={2}", fps, width, height);

  cv::VideoWriter writer;
  int ex = static_cast<int>(capture.get(cv::CAP_PROP_FOURCC));
  writer.open(outputPath, ex, fps, cv::Size(width, height));
  cv::Mat frame;
  ASSERT_TRUE(capture.isOpened());
  int nb_updates = 0;
  bool initialized = false;

  for (int i = 0; i < nb_frames; i++) {
    //spdlog::info(i);
    capture >> frame;

    EXPECT_LE(all_detections.at(i).size(), 1);
    PositionInFrame detection;

    if (!all_detections.at(i).empty() && !initialized) {
      detection = all_detections.at(i).at(0);
      tk.Init(detection);

      vector<PositionInFrame> predictions = {detection};
      trackerPredictions.push_back(predictions);
      initialized = true;
    } else {
      //tk.CorrectTime(dT);
      PositionInFrame prediction = tk.Predict(dT);

      // TODO handle multi objects prediction
      vector<PositionInFrame> predictions = {prediction};

      trackerPredictions.push_back(predictions);
      cv::rectangle(frame, prediction.toRect(), cv::Scalar(0, 255, 0), 1, 8, 0);
      displayFrameNb(frame, i+1);

      writer.write(frame);
      if (all_detections.at(i).size() > 0 &&
          (i % detectionsFrequency == 0 || i < 10)) {
        detection = all_detections.at(i).at(0);
        tk.Update(detection);
        nb_updates++;
      }
    }
  }
  writer.release();
  capture.release();
  spdlog::info("did {} updates for kalman in this run", nb_updates);
}

template <typename T>
void runTest(GeneralMonoTracker<T>tk , string testName, int detectionsFrequency, string videoPath,
             string outputPath, double EPS) {
  spdlog::info("starting {} for mono object tracking", testName);

  vector<vector<PositionInFrame>> trackerPredictions;
  spdlog::info("generating predictions using the tracker");
  getTrackerPredictions(tk, videoPath, outputPath, detectionsFrequency,
                        trackerPredictions);
  spdlog::info("got {} predictions", trackerPredictions.size());

  spdlog::info("testing the predictions against detections");
  PositionInFrame detection;
  PositionInFrame prediction;

  vector<double> iou_s = vector<double>();
  for (int i = 0; i < all_detections.size(); i++) {
    EXPECT_LE(all_detections.at(i).size(), 1);
    if (all_detections.at(i).size() > 0) {
      detection = all_detections.at(i).at(0);

      EXPECT_EQ(trackerPredictions.at(i).size(), 1);
      prediction = trackerPredictions.at(i).at(0);

      double iou = getIOU(prediction, detection);
      //EXPECT_GE(iou, EPS);
      iou_s.push_back(iou);

    }
  }
  spdlog::info("found mean ious {}", accumulate(iou_s.begin(), iou_s.end(), 0.)/iou_s.size());
  EXPECT_GE(accumulate(iou_s.begin(), iou_s.end(), 0.)/iou_s.size(), EPS);
}

TEST(utils, iou) {
  PositionInFrame position1(2, 2, 2, 2);
  PositionInFrame position2(3, 3, 2, 2);

  double iou = getIOU(position1, position2);
  EXPECT_EQ(iou, 1/7.);
}

TEST(monoObject, test_linear_0) {
  spdlog::info("importing detections");
  setup();

  TrackerConfig config = TrackerConfig();
  GeneralMonoTracker tk = GeneralMonoTracker<KalmanLinearConstantVelocity>(0, config.getCovParamsValues());
  runTest(tk, "test0", 1, videoPath,
          "/tmp/data/output_test0.mp4", 0.9); // 20% of detections are missing
}

TEST(monoObject,  test_linear_1) {
  spdlog::info("importing detections");
  setup();

  TrackerConfig config = TrackerConfig();
  GeneralMonoTracker tk = GeneralMonoTracker<KalmanLinearConstantVelocity>(0, config.getCovParamsValues());
  runTest(tk, "test1", 10, videoPath,
          "/tmp/data/output_test1.mp4", 0.8);
}

TEST(monoObject,  test_linear_2) {
  spdlog::info("importing detections");
  setup();

  TrackerConfig config = TrackerConfig();
  GeneralMonoTracker tk = GeneralMonoTracker<KalmanLinearConstantVelocity>(0, config.getCovParamsValues());
  runTest(tk,"test1", 25, videoPath,
          "/tmp/data/output_test2.mp4", 0.6);
}

TEST(monoObject, test_ekf_0) {
  spdlog::info("importing detections");
  setup();

  TrackerConfig config = TrackerConfig("../tracker/config/ekf_tracker_conf.ini");
  GeneralMonoTracker tk = GeneralMonoTracker<KalmanExtendedCTRV>(0, config.getCovParamsValues());
  runTest(tk, "test0", 1, videoPath,
          "/tmp/data/output_test0.mp4", 0.9); // 20% of detections are missing
}

TEST(monoObject,  test_ekf_1) {
  spdlog::info("importing detections");
  setup();

  TrackerConfig config = TrackerConfig("../tracker/config/ekf_tracker_conf.ini");
  GeneralMonoTracker tk = GeneralMonoTracker<KalmanExtendedCTRV>(0, config.getCovParamsValues());
  runTest(tk, "test1", 10, videoPath,
          "/tmp/data/output_test1.mp4", 0.8);
}

TEST(monoObject,  test_ekf_2) {
  spdlog::info("importing detections");
  setup();

  TrackerConfig config = TrackerConfig("../tracker/config/ekf_tracker_conf.ini");
  GeneralMonoTracker tk = GeneralMonoTracker<KalmanExtendedCTRV>(0, config.getCovParamsValues());
  runTest(tk,"test1", 25, videoPath,
          "/tmp/data/output_test2.mp4", 0.6);
}
