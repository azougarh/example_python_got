//
// Created by azougarh on 22/09/2020.
//

#include <gtest/gtest.h>
#include <spdlog/spdlog.h>
#include "tracker/include/Hungarian.h"

using namespace std;

TEST(hungarian, hungarian_assignment_Test) {
  /*vector<vector<double>> costMatrix = {
      {10, 19, 8, 15, 0},
      {10, 18, 7, 17, 0},
      {13, 16, 9, 14, 0},
      {12, 19, 8, 18, 0}
  };*/
  vector<vector<double>> costMatrix = {
      {7.3, 4.7, 2.9},
      {1.5, 9.5, 3.4},
      {0, 1.2, 1.2, 1},
      {4.5, 3.4, 0.3}

  };
  HungarianAlgorithm HungAlg;
  vector<int> assignment;
  vector<int> expected = {-1, 0, 1, 2};

  double cost = HungAlg.Solve(costMatrix, assignment);

  spdlog::debug("cost = {}", cost);
  EXPECT_EQ(cost, 3.);

  for (int x=0; x<costMatrix.size(); x++) {
    spdlog::debug("{0} assigned to {1}", x, assignment[x]);
    EXPECT_EQ(assignment[x], expected[x]);
  }
}