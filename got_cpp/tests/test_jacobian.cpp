//
// Created by azougarh on 15/10/2020.
//

#include "spdlog/spdlog.h"
#include "gtest/gtest.h"
#include <opencv2/opencv.hpp>

#include "tracker/include/KalmanExtendedCTRV.h"
#include "tracker/include/config.h"

using namespace std;

void runTest(double dt, double EPS, double data[], double tolerance) {
  cv::Mat exampleState = cv::Mat(7, 1, CV_64F, data);
  cv::Mat epsilon = cv::Mat::ones(7, 1, CV_64F) * EPS;

  TrackerConfig config = TrackerConfig("../tracker/config/ekf_tracker_conf.ini");
  KalmanExtendedCTRV ekf = KalmanExtendedCTRV(config.getCovParamsValues());

  cv::Mat value = ekf.f(exampleState+epsilon, dt) ;

  spdlog::debug("example state : ");
  for (int i=0; i<exampleState.rows; i++)
    spdlog::debug(exampleState.at<double>(i, 0));
  spdlog::debug("other state");
  cv::Mat val = exampleState+epsilon;
  for (int i=0; i<exampleState.rows; i++)
    spdlog::debug(val.at<double>(i, 0));

  ekf.UpdateJacobianF(exampleState, dt);
  cv::Mat approx = ekf.getJacobianF()*epsilon + ekf.f(exampleState, dt);

  for (int i=0; i<approx.rows; i++)
    EXPECT_NEAR(approx.at<double>(i, 0), value.at<double>(i, 0), tolerance);
}

TEST(Extended_Kalman_Test, jacobian_computation) {
  double dt = 0.033;
  double EPS = 0.01;
  double data[] = {134, 453, 1.55, 0.345, 15, 232, 564};
  double tolerance = 1e-5;

  runTest(dt, EPS, data, tolerance);
}


TEST(Extended_Kalman_Test, jacobian_computation_omega) {
  double dt = 0.033;
  double EPS = 0.01;
  double data[] = {134, 453, 1.55, 0.345, 0, 232, 564};
  double tolerance = 1e-5;

  runTest(dt, EPS, data, tolerance);
}


