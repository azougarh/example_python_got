#!/bin/bash

NAME=container-got
IMAGE=dev-got
docker run -it -d -v $PWD/../../got_cpp:/home/user/got_cpp --cap-add sys_ptrace -p127.0.0.1:2223:22 --name ${NAME} ${IMAGE}