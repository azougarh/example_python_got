#!/bin/bash

DIR=$(dirname $0)
CI=${1:-sail}

fly -t $CI set-pipeline -p 'got_cpp' -c $DIR/pipeline.yml -l $DIR/params.yaml
echo "fly -t sail unpause-pipeline -p '${PROJECT_NAME}'"